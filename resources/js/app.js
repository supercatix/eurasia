const { default: Axios } = require('axios');
const { max } = require('lodash');

require('./bootstrap');
var page = 2;

window.onload= function () {
    let buttonMore= document.getElementById('loadPages')
    buttonMore.addEventListener('click', function(e){
        $(".pagination_btn").text('Загружаю...');
        let category_id = buttonMore.getAttribute('data-category-id')
        loadMore(category_id)
    });
}


function loadMore(category_id) {
    axios.get('/categories/'+category_id+'/load', {
        params: {
            'page': page
        }
    })
    .then(response => {
        console.log(response.data);
        response.data.articles.data.forEach(article => {
            document.getElementById('loadPage').innerHTML += `
            <div class="newsItem__column" id="loadPage">
            <div class="newsItem_item">
                    <img src="/storage/${article.image}" alt="">
                    <h3 style="">${article.created_at.substr(0,10)}</h3>
                    <div class="line"></div>
                    <h2><a href="/articles/${article.id}">${article.title}</a></h2>
                </div>
                </div>`
                $(".pagination_btn").text('Показать еще');
        });
        if (page >= response.data.articles.last_page) {
            $(".pagination_btn").remove();
            console.log(page, response.data.articles.last_page)
        }
        page = page +1;
    })
}

$('.newsSlider_sliderr').slick({
    infinite: true,
    speed: 300,
    slidesToShow: 1,
    adaptiveHeight: true,
    arrows: true,
    nextArrow: '<button type="button" class="myArrow" style="outline:none;"><img src="/assets/img/next.svg" alt=""></button>',
    prevArrow: '<button type="button" class="BackArrow" style="outline:none;"><img src="/assets/img/prev.svg" alt=""></button>',
  })

  $('.personalSlider_bg').slick({
    infinite: true,
    slidesToShow: 1,
    arrows: true,
    nextArrow: '<button type="button" class="secondArrow" style="outline:none;"><img src="/assets/img/next_personal.svg" alt=""></button>',
    prevArrow: '<button type="button" class="firstArrow" style="outline:none;"><img src="/assets/img/prev_personal.svg" alt=""></button>',
  })

$('.main_slider').slick({
    arrows: true,
    infinite: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    prevArrow: $('.prev'),
    nextArrow: $('.next'),
    dots: true,
  })
