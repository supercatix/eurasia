@include('layouts.header')
        {{-- @dd($slides) --}}
        <section class="main_slider_wrap">
            <div class="container-fluid">
              <div class="row">
                <div class="col-xl-12 col-md-12 col-12 col-sm-12" style="padding: 0px">
                  <div class="prev">
                    <img src="/assets/img/prev.svg" alt="">
                  </div>
                  <div class="main_slider">
                    @foreach ($slides as $slide)
                        <div class="main_slider_item">
                            <img src="/storage/{{ $slide->image }}" alt="">
                        </div>
                    @endforeach
                  </div>
                  <div class="next">
                    <img src="/assets/img/next.svg" alt="">
                  </div>
                </div>
              </div>
            </div>
        </section>
          <section class="journalSlider">
            <div class="container-fluid">
              <div class="row">
                <div class="col-xl-12" style="padding: 0px;">
                  <div class="journalSlider__title">
                    <h2>{{ setting('journals.first') }}</h2>
                  </div>
                </div>
                <div class="col-xl-12" style="padding: 0px;">
                  <div class="journalSlider__slider">
                    @foreach($journals as $journal)
                      <img src="/storage/{{ $journal->image }}" alt="">
                        @endforeach
                  </div>
                </div>
                <div class="col-xl-12" style="padding: 0px;">
                  <div class="dots_wrap">
                    <div class="dots"></div>
                  </div>
                </div>
              </div>
            </div>
          </section>
          @foreach ($categories as $category)
          @if($loop->iteration % 2 != 0 && $loop->iteration != 4)
          <div>
          <section class="newsSlider">
            <div class="container-fluid container_wrapper" style="">
              <div class="row">
              <div class="col-xl-12" style="padding: 0;">
                  <div class="newsSlider__title">
                    <h2>{{ $category->title }}</h2>
                  </div>
                </div>
              </div>
            </div>
            <div class="newsSlider_bg">
              <div class="container-fluid container_wrapper">
                <div class="row">
                  <div class="col-xl-12" style="padding: 0;">
                    <div class="newsSlider_sliderr" id="slider_1">
                        @foreach ($category->populars as $popular)
                      <div class="newsSlider_slider_item">
                        <img src="/storage/{{ $popular->article->image }}" alt="">
                        <div class="newsSlider_slider_item_descr">
                          <h3>{{ \Carbon\Carbon::parse($popular->article->created_at)->toDateString() }}</h3>
                          <div class="line"></div>
                        <h2><a href="/articles/{{$popular->article->id}}">{{$popular->article->title}}</a></h2>
                        </div>
                      </div>
                      @endforeach
                    </div>
                    <div class="arrows">
                    {{-- <div class="prev news_prev">
                      <img src="/assets/img/prev.svg" alt="">
                    </div> --}}
                    {{-- <div class="next news_next">
                      <img src="/assets/img/next.svg" alt="">
                    </div> --}}
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </section>
          <section class="newsItem container_wrapper" style="">
            <div class="container-fluid">
              <div class="row">
                @foreach ($category->articles()->inRandomOrder()->limit(3)->get() as $article)
                    <div class="col-xl-4 col-12" style="padding: 0;">
                        <div class="newsItem_item">
                            <img src="/storage/{{ $article->image }}" alt="">
                            <h3 style='color: #056496;'>{{ \Carbon\Carbon::parse($article->created_at)->toDateString() }}</h3>
                            <div class="line"></div>
                            <h2><a href="/articles/{{$article->id}}">{{$article->title}}</a></h2>
                        </div>
                    </div>
                @endforeach
              </div>
            </div>
          </section>
        </div>
        @else
        @if($loop->iteration == 4)
        <section class="main_slider_wrap">
            <div class="container-fluid">
              <div class="row">
                <div class="col-xl-12 col-md-12 col-12 col-sm-12" style="padding: 0px">
                  <div class="prev reverseSlider_prev">
                    <img src="/assets/img/prev.svg" alt="">
                  </div>
                  <div class="reverseSlider_slider">
                      @foreach ($slides as $slide)
                          <div class="main_slider_item">
                              <img src="/storage/{{ $slide->image }}" alt="">
                          </div>
                      @endforeach
                  </div>
                  <div class="next reverseSlider_next">
                    <img src="/assets/img/next.svg" alt="">
                  </div>
                </div>
              </div>
            </div>
        </section>

        @endif
          <section class="personalSlider">
            <div class="container-fluid container_wrapper">
              <div class="row">
                <div class="col-xl-12" style="padding: 0;">
                  <div class="newsSlider__title">
                    <h2>{{ $category->title }}</h2>
                  </div>
                </div>
              </div>
            </div>
              <div class="container-fluid personalSlider_bg">
                @foreach ($category->populars as $popular)
                <div class="personalSlider_item">
                  <div class="personalSlider_img">
                    <img src="/storage/{{$popular->article->image}}" alt="">
                  </div>
                  <div class="personalSlider_descr">
                    <h3>{{ \Carbon\Carbon::parse($popular->article->created_at)->toDateString() }}</h3>
                    <div class="line"></div>
                    <h2>{{$popular->article->title}}</h2>
                  </div>
                </div>
                @endforeach
                </div>
              </div>
              <div class="personalSlider_arrows">
              </div>
            </div>
          </section>
          <section class="newsItem container_wrapper" style="">
            <div class="container-fluid">
              <div class="row">
                @foreach ($category->articles()->inRandomOrder()->limit(3)->get() as $article)
               <div class="col-xl-4 col-12" style="padding: 0;">
               <div class="newsItem_item">
                    <img src="/storage/{{ $article->image }}" alt="">
                    <h3 style='color: #ECAC00;'>{{ \Carbon\Carbon::parse($article->created_at)->toDateString() }}</h3>
                    <div class="line"></div>
                    <h2><a href="/articles/{{$article->id}}">{{ $article->title }}</a></h2>
                  </div>
                </div>
                @endforeach
              </div>
            </div>
          </section>
          @endif
          @endforeach
          <div class="footer_nav">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-xl-12" style="padding: 0;">
                        <div class="footer_nav_child">
                            <ul>
                                @foreach ($categories as $category)
                                <li>
                                <a href="/categories/{{ $category->id }}">{{ $category->title }}</a>
                                </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @include('layouts.footer')
