  @include('layouts.header')
    <main>
        <div class="container-fluid" style="border-top: 1px solid rgba(0, 0, 0, 0.2);">
            <div class="row">
                <div class="col-xl-5 col-md-12 col-sm-12 col-12" style="display: flex;">
                    <div class="contacts_descr">
                        <div class="contacts_descr_text">
                            <div class="contacts_descr_text_1">
                                <span>Головной офис:</span>
                                <a href="tel:{{ setting('contacts.number') }}">{{setting('contacts.number')}}</a>
                            </div>
                            <div class="contacts_descr_text_2">
                                <span>Почта:</span>
                                <a href="mailto:{{ setting('contacts.email') }}">{{ setting('contacts.email') }}</a>
                            </div>
                            <div class="line"></div>
                        </div>
                        <div class="contacts_descr_form">
                            <h2>Оставьте заявку и мы свяжемся с вами</h2>
                            <form>
                                <input type="text" placeholder="Ваше имя" />
                                <input type="text" placeholder="Телефон" />
                                <input type="text" placeholder="Почта" />
                                <button type="submit">Отправить <img src="{{asset('assets/img/right_arrow.svg')}}" alt=""></button>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="col-xl-7 col-md-12 col-sm-12 col-12" style="padding: 0;">
                    <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d3556.1534532602504!2d76.96421829693915!3d43.267624984527856!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sru!2skz!4v1603451634844!5m2!1sru!2skz" width="100%" height="100%" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
                </div>
            </div>
        </div>
    </main>
        {{-- @include('layouts.footer') --}}
    <script src="{{asset('dist/js/main.js')}}"> </script>
    </body>
</html>
