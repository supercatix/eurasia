@include('layouts.header')
        <section class="newsSlider">
            <div class="container-fluid container_wrapper" style="">
              <div class="row">
              <div class="col-xl-12" style="padding: 0;">
                  <div class="newsSlider__title">
                    <h2>{{ $category->title }}</h2>
                  </div>
                </div>
              </div>
            </div>
            <div class="newsSlider_bg">
              <div class="container-fluid container_wrapper">
                <div class="row">
                  <div class="col-xl-12" style="padding: 0;">
                    <div class="newsSlider_slider">
                        @foreach($populars as $popular)
                      <div class="newsSlider_slider_item">
                        <img src="/storage/{{$popular->article->image}}" alt="">
                        <div class="newsSlider_slider_item_descr">
                          <h3>{{ \Carbon\Carbon::parse($popular->article->created_at)->toDateString() }}</h3>
                          <div class="line"></div>
                          <h2>{{$popular->article->title}}</h2>
                        </div>
                      </div>
                      @endforeach
                    </div>
                    <div class="news_prev">
                      <img src="{{asset("assets/img/prev.svg")}}" alt="">
                    </div>
                    <div class="news_next">
                      <img src="{{asset("assets/img/next.svg")}}" alt="">
                    </div>
                  </div>
                </div>
              </div>
            </div>
        </section>
          <section class="newsItem container_wrapper" style="background: #F0FAFF;">
            <div class="container-fluid">
              <div class="newsItem__row" id="loadPage">
                    @foreach($articles as $article)
                    <div class="newsItem__column">
                   <div class="newsItem_item">
                            <img src="/storage/{{ $article->image }}" alt="">
                            <h3 style="">{{ \Carbon\Carbon::parse($article->created_at)->toDateString() }}</h3>
                            <div class="line"></div>
                     <h2><a href="/articles/{{$article->id}}">{{ $article->title }}</a></h2>
                     </div>
                    </div>
                    @endforeach
                </div>
                </div>
              </div>
            </div>
          </section>
        <button class="pagination_btn" id="loadPages" data-category-id='{{$category->id}}'>
            <img src="/assets/img/plus.svg" alt="">
            <span>Показать еще</span>
        </button>
        @include('layouts.footer')
