
    @include('layouts.header')
    <div style="background: #F0FAFF;">
        <section class="journalSlider">
            <div class="container-fluid">
              <div class="row">
                <div class="col-xl-12" style="padding: 0px;">
                  <div class="journalSlider__title">
                    <h2>{{ setting('journals.distribution') }}</h2>
                  </div>
                </div>
                <div class="col-xl-12" style="padding: 0px;">
                  <div class="journalSlider__slider">
                      @foreach($journals as $journal)
                  <img src="/storage/{{ $journal->image }}" alt="{{ $journal->title }}">
                    @endforeach
                  </div>
                </div>
                <div class="col-xl-12" style="padding: 0px;">
                  <div class="dots_wrap">
                    <div class="dots"></div>
                  </div>
                </div>
              </div>
            </div>
          </section>
    </div>
        <div class="container_wrapper">
            <div class="diagram">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-xl-12">
                            <h2 class="diagram__title">{{ setting('about.title_first') }}</h2>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xl-12">
                            <div class="diagram__descr">
                                <div class="logo_1_img">
                                    <img src="{{asset('/assets/img/ellipse.svg')}}" alt="">
                                </div>
                                <div class="logo_1">
                                    <img src="/storage/{{setting('about.left')}}" alt="">
                                </div>
                                <div class="logo_2_img">
                                    <img src="{{asset('/assets/img/ellipse.svg')}}" alt="">
                                </div>
                                <div class="logo_2">
                                    <img src="/storage/{{setting('distribution.right')}}" alt="">
                                </div>
                                <div class="plus">
                                    <h2>+</h2>
                                </div>
                            </div>
                            <div class="diagram_img">
                                <img src="{{asset('/assets/img/diagram_mob.svg')}}" alt="">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="advantage_wrap">
            <div class="advantage">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-xl-12">
                            <div class="advantage__title">
                                <h2>{{ setting('about.title_second') }}</h2>
                            </div>
                            <div class="advantage__footer_text">
                                <h2>{{ setting('about.description_second') }}</h2>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container_wrapper advantage__foot">
                <div class="row">
                    <div class="col-xl-4">
                        <div class="advantage__footer_wrap">
                            <p>{{ setting('about.first_column') }}</p>
                        </div>
                    </div>
                    <div class="col-xl-4">
                        <div class="advantage__footer_wrap">
                            <p>{{ setting('about.second_column') }}</p>
                        </div>
                    </div>
                    <div class="col-xl-4">
                        <div class="advantage__footer_wrap">
                            <p>{{ setting('about.third_column') }}</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container_wrapper" style="background: #F0FAFF; margin-top: 7.8125vw;">
            <div class="article">
                <div class="article_descr">
                    <h2>{{setting('about.third_title')}}</h2>
                </div>
            </div>
        </div>
        <div class="container_wrapper">
            <div class="our_project">
                <h3>Наши издания можно найти: </h3>
                <div class="container-fluid">
                    <div class="projects__row">
                        @foreach ($editions as $edition)
                        <ul>
                        <div class="projects__column">
                            <li>{{$edition->name}}</li>
                        </div>
                    </ul>
                        @endforeach
                    </div>
                        {{-- @endfor --}}
                </div>
            </div>
        </div>
    </div>
    <div class="container_wrapper">
        <div class="container-fluid">
            <div class="row">
                @foreach ($distributionImagesTop as $distributionImageTop)
                <div class="col-xl-4 gallery_img" style="padding-left: 0;">
                    <img src="/storage/{{$distributionImageTop->image}}" alt="">
                </div>
                @endforeach
                <div class="col-xl-8 gallery_img_big">
                    <img src="/storage/{{$distributionImageLeft->image}}" alt="">
                </div>
                @foreach ($distributionImagesRight as $distributionImageRight)
                <div class="col-xl-4 gallery_img_right" style="padding-left: 0;">
                    <img src="/storage/{{$distributionImageRight->image}}" alt="">
                </div>
                @endforeach
            </div>
        </div>
    </div>
    <button class="pagination_btn">
        <img src="/assets/img/plus.svg" alt="">
        <span>Показать еще</span>
    </button>
    <div class="container_wrapper" style="border-top: 1px solid rgba(0, 0, 0, 0.2);">
        <div class="row">
            <div class="col-xl-12">
                <div class="five_descr">
                    <h2>{{ setting('about.last_section') }}</h2>
                </div>
            </div>
        </div>
    </div>
        @include('layouts.footer')
