
 @include('layouts.header')
<div class="news_inner_bg" style="background-image: url(/storage/{{ $article->big_image }}); background-size: 100% 100%;">
     <div class="container-fluid">
         <div class="row">
             <div class="col-xl-12" style="padding: 0;">

             </div>
         </div>
     </div>
 </div>
 <div class="news_inner_text">
     <div class="container_wrapper">
         <div class="row">
             <div class="col-xl-8">
                 <div class="news_inner_breadscrump">
                     <ul>
                         <li>{{ $article->category->title }}</li>
                         <li>{{ \Carbon\Carbon::parse($article->created_at)->toDateString() }}</li>
                     </ul>
                 </div>
                 <div class="news_inner_title">
                     <h2>{{ $article->title }}</h2>
                 </div>
             </div>
         </div>
         <div class="row">
             <div class="col-xl-8 news_inner_text_descr">
                 {!! $article->description !!}
             </div>
             <div class="col-xl-4">
                 <div class="news_inner_text_items">
                     <h2>Последние статьи по теме</h2>
                     @foreach($last as $related)
                     @php
                        if(is_null($related)){
                            continue;
                        }
                    @endphp
                    <div class="news_inner_text_items_a">
                     <img src="/storage/{{ $related->image }}" alt="{{ $related->title }}">
                         <span>{{ \Carbon\Carbon::parse($related->created_at)->toDateString() }}</span>
                         <div class="line"></div>
                     <p> <a href="{{$related->id}}">{{ $related->title }}</a></p>
                     </div>
                     @endforeach
                     <div class="news_inner_text_items_bottom">
                         <h2>Поделись с друзьями</h2>
                         <div class="news_inner_text_items_bottom_social">
                             <ul>
                                 <li>
                                     <a href="javascript:void(0);">
                                         <img src="{{asset('assets/img/vk.svg')}}" alt="">
                                     </a>
                                 </li>
                                 <li>
                                     <a href="javascript:void(0);">
                                         <img src="{{asset('assets/img/twitter.svg')}}" alt="">
                                     </a>
                                 </li>
                                 <li>
                                     <a href="javascript:void(0);">
                                         <img src="{{asset('assets/img/instagram.svg')}}" alt="">
                                     </a>
                                 </li>
                                 <li>
                                     <a href="javascript:void(0);">
                                         <img src="{{asset('assets/img/facebook.svg')}}" alt="">
                                     </a>
                                 </li>
                             </ul>
                         </div>
                     </div>
                 </div>
             </div>
         </div>
     </div>
 </div>
 <div class="container-fluid container_wrapper" style="background-color: #F0FAFF;">
     <div class="row">
         <div class="col-xl-12">
             <div class="statiaSlider__title">
                 <h2>Похожие статьи</h2>
             </div>
         </div>
     </div>
 </div>
 <section class="newsItem container_wrapper" style="background-color:#F0FAFF;">
     <div class="container-fluid">
       <div class="row">
        @foreach($article->similar as $similar)
         <div class="col-xl-4 col-12" style="padding: 0;">
             @php
                     if(is_null($similar->article)){
                        continue;
                     }
            @endphp
           <div class="newsItem_item">
             <img src="/storage/{{ $similar->article->image }}" alt="{{ $similar->article->title }}">
             <h3 style="color: #ECAC00;">{{ \Carbon\Carbon::parse($similar->article->created_at)->toDateString() }}</h3>
             <div class="line"></div>
             <h2><a href="/articles/{{$similar->article->id}}">{{ $similar->article->title }}</a></h2>
           </div>
         </div>
         @endforeach
       </div>
     </div>
   </section>
 @include('layouts.footer')
