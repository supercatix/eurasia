<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="{{asset('dist/css/main.css')}}">
        <title>Eurasia</title>
    </head>
    <body class="antialiased">
        <div class="container_wrapper">
<header>
    <div class="container-fluid">
      <div class="row">
        <div class="col-xl-4 col-md-4 col-4 col-sm-4" style="padding: 0px">
          <a href="/" class="logo">
            <img src="{{asset("assets/img/logo.svg")}}" alt="Евразия">
          </a>
        </div>
        <div class="col-xl-8 col-md-8 col-8 col-sm-8" style="padding: 0px">
          <div class="nav_wrap">
            <nav>
              <ul>
                <li>
                  <a href="/categories/3">Персоны</a>
                </li>
                <li>
                  <a href="/categories/4">Путешествия</a>
                </li>
                <li>
                  <a href="/categories/5">История</a>
                </li>
                <li>
                  <a href="/categories/6">Развлечения</a>
                </li>
                <li>
                  <a href="/categories/9">Звезды</a>
                </li>
                <li>
                    <a href="/contacts">Контакты</a>
                  </li>
                <li class="burger_wrap">
                  <a href="javascript:void(0);">
                    <div class="burger">
                      <img src="{{asset("assets/img/burger.svg")}}" alt="">
                    </div>
                  </a>
                </li>
              </ul>
            </nav>
          </div>
        </div>
      </div>
    </div>
  </header>

  <div class="nav-panel-mobil">
      <div class="container">
          <nav class="nav-panel-mobil-ul">
        {{ menu('Header') }}
          </nav>
      </div>
  </div>
</div>
