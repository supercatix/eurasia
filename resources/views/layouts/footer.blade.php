<footer>
    <div class="container-fluid container_wrapper">
        <div class="row">
            <div class="col-xl-2 col-md-2 col-sm-12 col-12">
                <a href="#" class="logo">
                    <img src="{{asset("assets/img/logo.svg")}}" alt="">
                </a>
            </div>
            <div class="col-xl-7 col-md-7 col-sm-12 col-7">
                <div class="airplane">
                    <img src="{{asset("assets/img/airplane.svg")}}" alt="">
                </div>
            </div>
            <div class="col-xl-3 col-md-3 col-sm-12 col-12" style="padding: 0px;">
                <div class="footer_descr">
                    <div class="footer_soc">
                        <a href="javascript:void(0);">
                            facebook
                        </a>
                        <a href="javascript:void(0);">
                            instagram
                        </a>
                    </div>
                    <div class="footer_span">
                        <span>EURASIA AIR 2020</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
<script src="{{asset('dist/js/main.js')}}"> </script>
<script src="{{asset('/js/app.js')}}"> </script>
</body>
</html>
