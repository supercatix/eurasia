@include('layouts.header')
<div style="background: #F0FAFF;">
    <section class="journalSlider">
        <div class="container-fluid">
          <div class="row">
            <div class="col-xl-12" style="padding: 0px;">
              <div class="journalSlider__title">
                <h2>{{ setting('journals.about') }}</h2>
              </div>
            </div>
            <div class="col-xl-12" style="padding: 0px;">
              <div class="journalSlider__slider">
                  @foreach($journals as $journal)
              <img src="/storage/{{ $journal->image }}" alt="">
                @endforeach
              </div>
            </div>
            <div class="col-xl-12" style="padding: 0px;">
              <div class="dots_wrap">
                <div class="dots"></div>
              </div>
            </div>
          </div>
        </div>
      </section>
</div>
        <div class="container_wrapper">
            <div class="diagram">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-xl-12">
                        <h2 class="diagram__title">{{ setting('distribution.title_second_distribution') }}</h2>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xl-12">
                            <div class="diagram__descr">
                                <div class="logo_1_img">
                                    <img src="{{asset('/assets/img/ellipse.svg')}}" alt="">
                                </div>
                                <div class="logo_1">
                                    <img src="/storage/{{ setting('distribution.left') }}" alt="">
                                </div>
                                <div class="logo_2_img">
                                    <img src="{{asset('/assets/img/ellipse.svg')}}" alt="">
                                </div>
                                <div class="logo_2">
                                    <img src="/storage/{{ setting('distribution.right') }}" alt="">
                                </div>
                                <div class="plus">
                                    <h2>+</h2>
                                </div>
                            </div>
                            <div class="diagram_img">
                                <img src="{{asset('/assets/img/diagram_mob.svg')}}" alt="">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="advantage_wrap">
            <div class="advantage">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-xl-12">
                            <div class="advantage__title">
                            <h2>{{ setting('distribution.title_third_distribution') }}</h2>
                            </div>
                            <div class="advantage__footer_text">
                            <h2>{{ setting('distribution.description_third_distribution') }}</h2>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container_wrapper advantage__foot">
                <div class="row">
                    <div class="col-xl-4">
                        <div class="advantage__footer_wrap">
                            <p>{{setting('distribution.column_first')}}</p>
                        </div>
                    </div>
                    <div class="col-xl-4">
                        <div class="advantage__footer_wrap">
                            <p>{{setting('distribution.column_second')}}</p>
                        </div>
                    </div>
                    <div class="col-xl-4">
                        <div class="advantage__footer_wrap">
                            <p>{{setting('distribution.column_third')}}</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container_wrapper distribute_wrap">
            <div class="row">
                <div class="col-xl-12">
                    <div class="distribution_items_title">
                        <h2>{{ setting('distribution.title_four_distribution') }}</h2>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xl-4">
                    <div class="distribution_items">
                        <div class="icon">
                            <img src="/storage/{{ setting('distribution.one_column_four_section_icon') }}" alt="">
                        </div>
                        <div class="title">
                            <h2>{{ setting('distribution.one_column_four_section_title') }}</h2>
                        </div>
                        <div class="descr">
                            <p>{{ setting('distribution.one_column_four_section_description') }}</p>
                        </div>
                    </div>
                </div>
                <div class="col-xl-4">
                    <div class="distribution_items">
                        <div class="icon">
                            <img src="/storage/{{ setting('distribution.two_column_four_section_icon') }}" alt="">
                        </div>
                        <div class="title">
                            <h2>{{ setting('distribution.two_column_four_section_title') }}</h2>
                        </div>
                        <div class="descr">
                            <p>{{ setting('distribution.two_column_four_section_description') }}</p>
                        </div>
                    </div>
                </div>
                <div class="col-xl-4">
                    <div class="distribution_items">
                        <div class="icon">
                            <img src="/storage/{{ setting('distribution.third_column_four_section_icon') }}" alt="">
                        </div>
                        <div class="title">
                            <h2>{{ setting('distribution.third_column_four_section_title') }}</h2>
                        </div>
                        <div class="descr">
                            <p>{{ setting('distribution.third_column_four_section_description') }}</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container_wrapper">
            <div class="row">
                <div class="col-xl-12">
                    <div class="distribution_items_title">
                        <h2>{{ setting('distribution.title_five') }}</h2>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xl-4">
                    <div class="distribution_items">
                        <div class="icon">
                            <img src="/storage/{{ setting('distribution.first_column_five_section_icon') }}" alt="">
                        </div>
                        <div class="title">
                            <h2>{{ setting('distribution.first_column_five_section_title') }}</h2>
                        </div>
                        <div class="descr">
                            <p>{{ setting('distribution.first_column_five_section_description') }}</p>
                        </div>
                    </div>
                </div>
                <div class="col-xl-4">
                    <div class="distribution_items">
                        <div class="icon">
                            <img src="/storage/{{setting('distribution.second_column_five_section_icon')}}" alt="">
                        </div>
                        <div class="title">
                            <h2>{{ setting('distribution.second_column_five_section_title') }}</h2>
                        </div>
                        <div class="descr">
                            <p>{{ setting('distribution.second_column_five_section_description') }}</p>
                        </div>
                    </div>
                </div>
                <div class="col-xl-4">
                    <div class="distribution_items">
                        <div class="icon">
                            <img src="/storage/{{setting('distribution.third_column_five_section_icon')}}" alt="">
                        </div>
                        <div class="title">
                            <h2>{{ setting('distribution.third_column_five_section_title') }}</h2>
                        </div>
                        <div class="descr">
                            <p>{{ setting('distribution.third_column_five_section_description') }}</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="professional">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-xl-12">
                        <div class="container_wrapper">
                            <h2 class="professional_title">
                                {{ setting('distribution.title_six') }}
                            </h2>
                        </div>
                    </div>
                </div>
                <div class="container_wrapper">
                    <div class="row">
                        <div class="col-xl-12">
                            <div class="professional_item">
                                <div class="professional_item_span">
                                    <span>01</span>
                                </div>
                                <div class="professional_item_p">
                                    <p>{{ setting('distribution.one_list') }}</p>
                                </div>
                                <div class="professional_item_span">
                                    <span>02</span>
                                </div>
                                <div class="professional_item_p_left">
                                    <p>{{ setting('distribution.two_list') }}</p>
                                </div>
                                <div class="professional_item_span">
                                    <span>03</span>
                                </div>
                                <div class="professional_item_p">
                                    <p>{{ setting('distribution.third_list') }}</p>
                                </div>
                                <div class="professional_item_span">
                                    <span>04</span>
                                </div>
                                <div class="professional_item_p_left">
                                    <p>{{ setting('distribution.four_list') }}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container_wrapper" style="border-bottom: 1px solid rgba(0, 0, 0, 0.2);">
            <div class="row">
                <div class="col-xl-12">
                    <div class="air_descr">
                        <h2>{{ setting('distribution.title_seven') }}</h2>
                        <p>{{ setting('distribution.description_seven') }}</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="container_wrapper">
            <div class="container-fluid">
                <div class="row">
                    @foreach ($aboutImagesTop as $aboutImageTop)
                    <div class="col-xl-4 gallery_img" style="padding-left: 0;">
                        <img src="/storage/{{$aboutImageTop->image}}" alt="">
                    </div>
                    @endforeach
                    <div class="col-xl-8 gallery_img_big">
                        <img src="/storage/{{$aboutImageLeft->image}}" alt="">
                    </div>
                    @foreach ($aboutImagesRight as $aboutImageRight)
                    <div class="col-xl-4 gallery_img_right" style="padding-left: 0;">
                        <img src="/storage/{{$aboutImageRight->image}}" alt="">
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
        <button class="pagination_btn">
            <img src="/assets/img/plus.svg" alt="">
            <span>Показать еще</span>
        </button>
        <div class="container_wrapper" style="border-top: 1px solid rgba(0, 0, 0, 0.2);">
            <div class="article">
                <div class="article_descr">
                    <h2>{{ setting('distribution.eighth_text') }}</h2>
                </div>
            </div>
        </div>
        @include('layouts.footer')
