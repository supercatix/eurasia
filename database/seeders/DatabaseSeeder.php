<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();
        // \App\Models\Category::factory(10)->create();
        // \App\Models\Article::factory(10)->create();
        // \App\Models\Journal::factory(10)->create();
        // \App\Models\Related::factory(10)->create();
        // \App\Models\Popular::factory(10)->create();
        // \App\Models\Similar::factory(10)->create();
        // \App\Models\Slide::factory(10)->create();
        // \App\Models\AboutImage::factory(10)->create();
            \App\Models\Edition::factory(10)->create();
    }
}
