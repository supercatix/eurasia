<?php

namespace Database\Factories;

use App\Models\Popular;
use App\Models\Article;
use App\Models\Category;
use Illuminate\Database\Eloquent\Factories\Factory;

class PopularFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Popular::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'article_id' => Article::inRandomOrder()->first()->id,
            'order' => random_int(0, 10),
            'category_id' => Category::inRandomOrder()->first()->id
        ];
    }
}
