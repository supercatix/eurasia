<?php

namespace Database\Factories;

use App\Models\Similar;
use App\Models\Article;
use Illuminate\Database\Eloquent\Factories\Factory;

class SimilarFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Similar::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'article_id' => Article::inRandomOrder()->first()->id,
            'similar_id' => Article::inRandomOrder()->first()->id
        ];
    }
}
