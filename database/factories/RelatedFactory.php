<?php

namespace Database\Factories;

use App\Models\Related;
use App\Models\Article;
use Illuminate\Database\Eloquent\Factories\Factory;

class RelatedFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Related::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'article_id' => Article::inRandomOrder()->first()->id,
            'related_id' => Article::inRandomOrder()->first()->id
        ];
    }
}
