<?php

namespace Database\Factories;

use App\Models\AboutImage;
use Illuminate\Database\Eloquent\Factories\Factory;
class AboutImageFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = AboutImage::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'image' => $this->faker->imageUrl()
        ];
    }
}
