<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Category extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = [
        'title'
    ];

    protected $table = 'categories_main';

    public function articles()
    {
        return $this->hasMany('App\Models\Article');
    }

    public function populars()
    {
        return $this->hasMany('App\Models\Popular');
    }
}
