<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Related extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = [
        'article_id',
        'related_id'
    ];

    public function article()
    {
        return $this->belongsTo('App\Models\Article', 'id', 'related_id');
    }
}
