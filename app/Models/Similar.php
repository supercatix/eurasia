<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Similar extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = [
        'article_id',
        'similar_id'
    ];

    public function article()
    {
        return $this->hasOne('App\Models\Article', 'id', 'similar_id');
    }
}
