<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Slide;
use App\Models\Journal;
use App\Models\Category;
use App\Models\AboutImage;
use App\Models\DistributionImage;
use App\Models\Edition;
use App\Models\Popular;

class MainController extends Controller
{
    public function index(Request $request)
    {
        $slides = Slide::get();
        $journals = Journal::get();
        $categories = Category::whereIn('id', Popular::select('category_id')->get())->get();
        $categories->load(['articles', 'populars.article']);
        $footerCats = Category::get();
        return view('main', [
            'slides' => $slides,
            'journals' => $journals,
            'categories' => $categories,
            'footerCats' => $footerCats
        ]);
    }

    public function distribution(Request $request)
    {
        $distributionImagesTop = DistributionImage::inRandomOrder()->limit(4)->get();
        $distributionImageLeft = DistributionImage::inRandomOrder()->first();
        $distributionImagesRight = DistributionImage::inRandomOrder()->limit(3)->get();
        $journals = Journal::get();
        $editions = Edition::get();

        return view('distribution', [
            'journals' => $journals,
            'distributionImagesTop' => $distributionImagesTop,
            'distributionImageLeft' => $distributionImageLeft,
            'distributionImagesRight' => $distributionImagesRight,
            'editions' => $editions
        ]);
    }

    public function about(Request $request)
    {
        $aboutImagesTop = AboutImage::inRandomOrder()->limit(4)->get();
        $aboutImageLeft = AboutImage::inRandomOrder()->first();
        $aboutImagesRight = AboutImage::inRandomOrder()->limit(3)->get();
        $journals = Journal::get();
        $editions = Edition::get();

        return view('about', [
            'journals' => $journals,
            'aboutImagesTop' => $aboutImagesTop,
            'aboutImageLeft' => $aboutImageLeft,
            'aboutImagesRight' => $aboutImagesRight,
            'editions' => $editions
        ]);
    }

    public function contacts(Request $request)
    {
        return view('contacts');
    }
}
