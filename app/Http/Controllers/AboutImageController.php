<?php

namespace App\Http\Controllers;

use App\Models\AboutImage;
use Illuminate\Http\Request;

class AboutImageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\AboutImage  $aboutImage
     * @return \Illuminate\Http\Response
     */
    public function show(AboutImage $aboutImage)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\AboutImage  $aboutImage
     * @return \Illuminate\Http\Response
     */
    public function edit(AboutImage $aboutImage)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\AboutImage  $aboutImage
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, AboutImage $aboutImage)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\AboutImage  $aboutImage
     * @return \Illuminate\Http\Response
     */
    public function destroy(AboutImage $aboutImage)
    {
        //
    }
}
