<?php

namespace App\Http\Controllers;

use App\Models\DistributionImage;
use Illuminate\Http\Request;

class DistributionImageController extends Controller

{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

     /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

     /**
     * Display the specified resource.
     *
     * @param  \App\Models\DistributionImage  $aboutImage
     * @return \Illuminate\Http\Response
     */
    public function show(DistributionImage $distributionImage)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\AboutImage  $aboutImage
     * @return \Illuminate\Http\Response
     */
    public function edit(DistributionImage $distributionImage)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\DistributionImage  $distributionImage
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, DistributionImage $ditributionImage)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\AboutImage  $aboutImage
     * @return \Illuminate\Http\Response
     */
    public function destroy(DistributionImage $distributionImage)
    {
        //
    }
}
