window.$ = window.jQuery = require("jquery");

require('popper.js');
require('bootstrap');
require('slick-slider');


$('.main_slider').slick({
  infinite: true,
  slidesToShow: 1,
  slidesToScroll: 1,
  prevArrow: $('.prev'),
  nextArrow: $('.next'),
  dots: true
})

$('.journalSlider__slider').slick({
  slidesToShow: 6,
  slidesToScroll: 6,
  dots: false,
  centerMode: true,
  arrows: false,
  autoplay: true,
  autoplaySpeed: 0,
  speed: 800,
  cssEase: 'linear',
  responsive: [
    {
        breakpoint: 1024,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 3,
          infinite: true,
          dots: false
        }
    },
    {
        breakpoint: 600,
        settings: {
            slidesToShow: 3,
            slidesToScroll: 3
        }
    },
        {
        breakpoint: 360,
        settings: {
            slidesToShow: 1,
            slidesToScroll: 1
        }
    }
  ]
})

$('.newsSlider_slider').slick({
  infinite: true,
  speed: 300,
  slidesToShow: 1,
  adaptiveHeight: true,
  arrows: true,
  prevArrow: $('.news_prev'),
  nextArrow: $('.news_next'),
})

$('.reverseSlider_slider').slick({
  infinite: true,
  arrows: true,
  speed: 300,
  slidesToShow: 1,
  adaptiveHeight: true,
  prevArrow: $('.reverseSlider_prev'),
  nextArrow: $('.reverseSlider_next'),
})

$('.personalSlider_bg').slick({
  infinite: true,
  slidesToShow: 1,
  arrows: true,
  prevArrow: $('.personal_prev'),
  nextArrow: $('.personal_next'),
})

$('.burger').click(function(e){
    e.preventDefault();
    $('.nav-panel-mobil').toggleClass('nav-panel-mobil-active');
    $('body').toggleClass('body-overflow');
});
