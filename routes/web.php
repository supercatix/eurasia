<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'App\Http\Controllers\MainController@index');

Route::get('/categories/{category}', 'App\Http\Controllers\CategoryController@index');

Route::get('/categories/{category}/load', 'App\Http\Controllers\CategoryController@pages');

Route::get('/articles/{article}', 'App\Http\Controllers\ArticleController@show');

Route::get('/distribution', 'App\Http\Controllers\MainController@distribution');

Route::get('/about', 'App\Http\Controllers\MainController@about');

Route::get('/contacts', 'App\Http\Controllers\MainController@contacts');


Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});
